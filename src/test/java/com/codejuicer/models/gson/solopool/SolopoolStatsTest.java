package com.codejuicer.models.gson.solopool;

import com.google.gson.Gson;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SolopoolStatsTest {

    private final String soloStats = "{\"blocksFound24\":109," +
            "\"charts\":[{\"x\":1533105600,\"y\":108888888},{\"x\":1533105000,\"y\":131519274}]," +
            "\"currentHashrate\":117777776," +
            "\"earnings\":[{\"period\":3600,\"amount\":44552307690,\"blocks\":6}]," +
            "\"hashrate\":126378557," +
            "\"payments\":[{\"amount\":7425000000,\"timestamp\":1532921667,\"tx\":\"0x128e437c15c4911b191641ef66565a771d614ed06cc4e55d278e5b6ace5c8450\"}]," +
            "\"paymentsTotal\":47,\"reward24\":809816047920," +
            "\"rewards\":[{\"height\":1715524,\"timestamp\":1533063695,\"amount\":7425000000,\"matured\":true,\"orphan\":false}]," +
            "\"stats\":{\"balance\":14850000000,\"blocksFound\":269,\"immature\":7426871100,\"lastShare\":1533105930,\"paid\":1956313556079,\"pending\":0}," +
            "\"workers\":{\"0\":{\"lastBeat\":1533105928,\"hr\":28888888,\"offline\":false,\"hr2\":28653828},\"All\":{\"lastBeat\":1533105930,\"hr\":88888888,\"offline\":false,\"hr2\":97724729}}," +
            "\"workersOffline\":0,\"workersOnline\":2,\"workersTotal\":2}\n";

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }
    
    @Test
    void canDeserialize() {
        Gson gson = new Gson();
        SolopoolStats jsondata = gson.fromJson(soloStats, SolopoolStats.class);
        
        assertEquals(1533105928,jsondata.getWorkers().get("0").getLastBeat());
    }
}