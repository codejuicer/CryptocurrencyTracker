package com.codejuicer.models.gson.coinmarketcap;

import com.google.gson.Gson;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CoinMarketCapTickerArrayTest {
    
    String tickerData = "{\n" +
            "  \"data\": [\n" +
            "    {\n" +
            "      \"id\": 217,\n" +
            "      \"name\": \"Bela\",\n" +
            "      \"symbol\": \"BELA\",\n" +
            "      \"website_slug\": \"belacoin\",\n" +
            "      \"rank\": 748,\n" +
            "      \"circulating_supply\": 37280426,\n" +
            "      \"total_supply\": 43563339,\n" +
            "      \"max_supply\": null,\n" +
            "      \"quotes\": {\n" +
            "        \"USD\": {\n" +
            "          \"price\": 0.0572040946,\n" +
            "          \"volume_24h\": 3755.6423073609,\n" +
            "          \"market_cap\": 2132593,\n" +
            "          \"percent_change_1h\": -6.9,\n" +
            "          \"percent_change_24h\": -11.88,\n" +
            "          \"percent_change_7d\": -18.72\n" +
            "        },\n" +
            "        \"BTC\": {\n" +
            "          \"price\": 0.0000075579,\n" +
            "          \"volume_24h\": 0.4962017365,\n" +
            "          \"market_cap\": 282,\n" +
            "          \"percent_change_1h\": -7.03,\n" +
            "          \"percent_change_24h\": -5.23,\n" +
            "          \"percent_change_7d\": -11.31\n" +
            "        }\n" +
            "      },\n" +
            "      \"last_updated\": 1533107786\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 218,\n" +
            "      \"name\": \"FlutterCoin\",\n" +
            "      \"symbol\": \"FLT\",\n" +
            "      \"website_slug\": \"fluttercoin\",\n" +
            "      \"rank\": 1116,\n" +
            "      \"circulating_supply\": 436450711,\n" +
            "      \"total_supply\": 436450711,\n" +
            "      \"max_supply\": null,\n" +
            "      \"quotes\": {\n" +
            "        \"USD\": {\n" +
            "          \"price\": 0.0011331729,\n" +
            "          \"volume_24h\": 64.7503290976,\n" +
            "          \"market_cap\": 494574,\n" +
            "          \"percent_change_1h\": 0.2,\n" +
            "          \"percent_change_24h\": null,\n" +
            "          \"percent_change_7d\": -8.88\n" +
            "        },\n" +
            "        \"BTC\": {\n" +
            "          \"price\": 1.497e-7,\n" +
            "          \"volume_24h\": 0.0085549217,\n" +
            "          \"market_cap\": 65,\n" +
            "          \"percent_change_1h\": 0.06,\n" +
            "          \"percent_change_24h\": null,\n" +
            "          \"percent_change_7d\": -0.57\n" +
            "        }\n" +
            "      },\n" +
            "      \"last_updated\": 1533107722\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 221,\n" +
            "      \"name\": \"OctoCoin\",\n" +
            "      \"symbol\": \"888\",\n" +
            "      \"website_slug\": \"octocoin\",\n" +
            "      \"rank\": 1276,\n" +
            "      \"circulating_supply\": 54944494,\n" +
            "      \"total_supply\": 54944494,\n" +
            "      \"max_supply\": null,\n" +
            "      \"quotes\": {\n" +
            "        \"USD\": {\n" +
            "          \"price\": 0.0018137479,\n" +
            "          \"volume_24h\": 32.3455069998,\n" +
            "          \"market_cap\": 99655,\n" +
            "          \"percent_change_1h\": 0.24,\n" +
            "          \"percent_change_24h\": -17.34,\n" +
            "          \"percent_change_7d\": -20.63\n" +
            "        },\n" +
            "        \"BTC\": {\n" +
            "          \"price\": 2.396e-7,\n" +
            "          \"volume_24h\": 0.0042735424,\n" +
            "          \"market_cap\": 13,\n" +
            "          \"percent_change_1h\": 0.1,\n" +
            "          \"percent_change_24h\": -11.1,\n" +
            "          \"percent_change_7d\": -13.39\n" +
            "        }\n" +
            "      },\n" +
            "      \"last_updated\": 1533107785\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 224,\n" +
            "      \"name\": \"FairCoin\",\n" +
            "      \"symbol\": \"FAIR\",\n" +
            "      \"website_slug\": \"faircoin\",\n" +
            "      \"rank\": 1044,\n" +
            "      \"circulating_supply\": 53193831,\n" +
            "      \"total_supply\": 53193831,\n" +
            "      \"max_supply\": null,\n" +
            "      \"quotes\": {\n" +
            "        \"USD\": {\n" +
            "          \"price\": 0.3318402999,\n" +
            "          \"volume_24h\": 16.5920149938,\n" +
            "          \"market_cap\": 17651857,\n" +
            "          \"percent_change_1h\": 0.24,\n" +
            "          \"percent_change_24h\": -4.17,\n" +
            "          \"percent_change_7d\": -8.12\n" +
            "        },\n" +
            "        \"BTC\": {\n" +
            "          \"price\": 0.0000438433,\n" +
            "          \"volume_24h\": 0.0021921647,\n" +
            "          \"market_cap\": 2332,\n" +
            "          \"percent_change_1h\": 0.1,\n" +
            "          \"percent_change_24h\": 3.06,\n" +
            "          \"percent_change_7d\": 0.26\n" +
            "        }\n" +
            "      },\n" +
            "      \"last_updated\": 1533107784\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 233,\n" +
            "      \"name\": \"SolarCoin\",\n" +
            "      \"symbol\": \"SLR\",\n" +
            "      \"website_slug\": \"solarcoin\",\n" +
            "      \"rank\": 535,\n" +
            "      \"circulating_supply\": 44730320,\n" +
            "      \"total_supply\": 98034486883,\n" +
            "      \"max_supply\": null,\n" +
            "      \"quotes\": {\n" +
            "        \"USD\": {\n" +
            "          \"price\": 0.1208737918,\n" +
            "          \"volume_24h\": 7435.2567289644,\n" +
            "          \"market_cap\": 5406723,\n" +
            "          \"percent_change_1h\": 0.14,\n" +
            "          \"percent_change_24h\": -7.02,\n" +
            "          \"percent_change_7d\": -29.96\n" +
            "        },\n" +
            "        \"BTC\": {\n" +
            "          \"price\": 0.00001597,\n" +
            "          \"volume_24h\": 0.9823585417,\n" +
            "          \"market_cap\": 714,\n" +
            "          \"percent_change_1h\": 0,\n" +
            "          \"percent_change_24h\": 0,\n" +
            "          \"percent_change_7d\": -23.57\n" +
            "        }\n" +
            "      },\n" +
            "      \"last_updated\": 1533107785\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 234,\n" +
            "      \"name\": \"e-Gulden\",\n" +
            "      \"symbol\": \"EFL\",\n" +
            "      \"website_slug\": \"e-gulden\",\n" +
            "      \"rank\": 832,\n" +
            "      \"circulating_supply\": 16923010,\n" +
            "      \"total_supply\": 20766477,\n" +
            "      \"max_supply\": 21000000,\n" +
            "      \"quotes\": {\n" +
            "        \"USD\": {\n" +
            "          \"price\": 0.0730789273,\n" +
            "          \"volume_24h\": 7148.1449956854,\n" +
            "          \"market_cap\": 1236715,\n" +
            "          \"percent_change_1h\": 7.1,\n" +
            "          \"percent_change_24h\": 4.49,\n" +
            "          \"percent_change_7d\": -0.93\n" +
            "        },\n" +
            "        \"BTC\": {\n" +
            "          \"price\": 0.0000096553,\n" +
            "          \"volume_24h\": 0.9444248598,\n" +
            "          \"market_cap\": 163,\n" +
            "          \"percent_change_1h\": 6.95,\n" +
            "          \"percent_change_24h\": 12.37,\n" +
            "          \"percent_change_7d\": 8.11\n" +
            "        }\n" +
            "      },\n" +
            "      \"last_updated\": 1533107783\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 254,\n" +
            "      \"name\": \"Gulden\",\n" +
            "      \"symbol\": \"NLG\",\n" +
            "      \"website_slug\": \"gulden\",\n" +
            "      \"rank\": 252,\n" +
            "      \"circulating_supply\": 401938640,\n" +
            "      \"total_supply\": 473438640,\n" +
            "      \"max_supply\": null,\n" +
            "      \"quotes\": {\n" +
            "        \"USD\": {\n" +
            "          \"price\": 0.0530793752,\n" +
            "          \"volume_24h\": 100136.178812865,\n" +
            "          \"market_cap\": 21334652,\n" +
            "          \"percent_change_1h\": -0.2,\n" +
            "          \"percent_change_24h\": -5.26,\n" +
            "          \"percent_change_7d\": -13.38\n" +
            "        },\n" +
            "        \"BTC\": {\n" +
            "          \"price\": 0.0000070129,\n" +
            "          \"volume_24h\": 13.2301592505,\n" +
            "          \"market_cap\": 2819,\n" +
            "          \"percent_change_1h\": -0.34,\n" +
            "          \"percent_change_24h\": 1.89,\n" +
            "          \"percent_change_7d\": -5.48\n" +
            "        }\n" +
            "      },\n" +
            "      \"last_updated\": 1533107786\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 257,\n" +
            "      \"name\": \"Polcoin\",\n" +
            "      \"symbol\": \"PLC\",\n" +
            "      \"website_slug\": \"polcoin\",\n" +
            "      \"rank\": 1310,\n" +
            "      \"circulating_supply\": 78554445,\n" +
            "      \"total_supply\": 78554445,\n" +
            "      \"max_supply\": 218500000,\n" +
            "      \"quotes\": {\n" +
            "        \"USD\": {\n" +
            "          \"price\": 0.0008313011,\n" +
            "          \"volume_24h\": 85.297987496,\n" +
            "          \"market_cap\": 65302,\n" +
            "          \"percent_change_1h\": 0.24,\n" +
            "          \"percent_change_24h\": 2.29,\n" +
            "          \"percent_change_7d\": -23.9\n" +
            "        },\n" +
            "        \"BTC\": {\n" +
            "          \"price\": 1.098e-7,\n" +
            "          \"volume_24h\": 0.0112697126,\n" +
            "          \"market_cap\": 9,\n" +
            "          \"percent_change_1h\": 0.1,\n" +
            "          \"percent_change_24h\": 10.01,\n" +
            "          \"percent_change_7d\": -16.96\n" +
            "        }\n" +
            "      },\n" +
            "      \"last_updated\": 1533107783\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 258,\n" +
            "      \"name\": \"Groestlcoin\",\n" +
            "      \"symbol\": \"GRS\",\n" +
            "      \"website_slug\": \"groestlcoin\",\n" +
            "      \"rank\": 158,\n" +
            "      \"circulating_supply\": 70576184,\n" +
            "      \"total_supply\": 70576184,\n" +
            "      \"max_supply\": 105000000,\n" +
            "      \"quotes\": {\n" +
            "        \"USD\": {\n" +
            "          \"price\": 0.5677830351,\n" +
            "          \"volume_24h\": 2258499.62033114,\n" +
            "          \"market_cap\": 40071960,\n" +
            "          \"percent_change_1h\": 0.48,\n" +
            "          \"percent_change_24h\": -9.98,\n" +
            "          \"percent_change_7d\": -7.89\n" +
            "        },\n" +
            "        \"BTC\": {\n" +
            "          \"price\": 0.0000750164,\n" +
            "          \"volume_24h\": 298.3967432995,\n" +
            "          \"market_cap\": 5294,\n" +
            "          \"percent_change_1h\": 0.34,\n" +
            "          \"percent_change_24h\": -3.19,\n" +
            "          \"percent_change_7d\": 0.51\n" +
            "        }\n" +
            "      },\n" +
            "      \"last_updated\": 1533107785\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": 260,\n" +
            "      \"name\": \"PetroDollar\",\n" +
            "      \"symbol\": \"XPD\",\n" +
            "      \"website_slug\": \"petrodollar\",\n" +
            "      \"rank\": 1119,\n" +
            "      \"circulating_supply\": 63993275,\n" +
            "      \"total_supply\": 63993275,\n" +
            "      \"max_supply\": null,\n" +
            "      \"quotes\": {\n" +
            "        \"USD\": {\n" +
            "          \"price\": 0.0074033961,\n" +
            "          \"volume_24h\": 84.0452689954,\n" +
            "          \"market_cap\": 473768,\n" +
            "          \"percent_change_1h\": 0.2,\n" +
            "          \"percent_change_24h\": -19.87,\n" +
            "          \"percent_change_7d\": -13.5\n" +
            "        },\n" +
            "        \"BTC\": {\n" +
            "          \"price\": 9.781e-7,\n" +
            "          \"volume_24h\": 0.0111042014,\n" +
            "          \"market_cap\": 63,\n" +
            "          \"percent_change_1h\": 0.06,\n" +
            "          \"percent_change_24h\": -13.82,\n" +
            "          \"percent_change_7d\": -5.61\n" +
            "        }\n" +
            "      },\n" +
            "      \"last_updated\": 1533107724\n" +
            "    }\n" +
            "  ],\n" +
            "  \"metadata\": {\n" +
            "    \"timestamp\": 1533107350,\n" +
            "    \"num_cryptocurrencies\": 1722,\n" +
            "    \"error\": null\n" +
            "  }\n" +
            "}";

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }
    
    @Test
    void canDeserialize() {
        Gson gson = new Gson();
        CoinMarketCapTickerArray ticker = gson.fromJson(tickerData, CoinMarketCapTickerArray.class);
        assertNotNull(ticker.getData().get(0).getQuotes().get("BTC"));
    }
}