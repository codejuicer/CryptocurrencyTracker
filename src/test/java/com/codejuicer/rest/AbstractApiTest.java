package com.codejuicer.rest;

import com.codejuicer.models.gson.coinmarketcap.CoinMarketCapTickerArray;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class AbstractApiTest {

    @BeforeEach
    void setUp() {
        
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getEndpointUrl() {
    }

    @Test
    void init() {
        try {
            FakeApi<CoinMarketCapTickerArray> api = mock(FakeApi.class);
            when(api.call()).thenReturn("Happy.");
            
            
        } catch(Exception e) {
            fail(e);
        }
            
    }

    @Test
    void call() {
    }

    @Test
    void withParameters() {
    }

    @Test
    void withMethod() {
    }

    @Test
    void withResource() {
    }
}