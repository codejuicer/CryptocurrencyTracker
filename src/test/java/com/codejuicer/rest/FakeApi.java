package com.codejuicer.rest;

public class FakeApi<T> extends AbstractApi {
 
    public FakeApi() { super(); }
    
    public FakeApi(Class<T> responseClassType) {
        super("https://some.fake.example.com/", responseClassType);
    }
}
