import com.codejuicer.dao.AbstractDao;
import com.codejuicer.dao.ListingsDao;
import com.codejuicer.models.gson.coinmarketcap.CoinMarketCapListing;
import com.codejuicer.models.gson.coinmarketcap.CoinMarketCapTickerArray;
import com.codejuicer.res.Queries;
import com.codejuicer.rest.coinmarketcap.CoinMarketCapEndpoint;
import com.codejuicer.rest.coinmarketcap.TickerParameters;
import com.codejuicer.util.ResourceManager;


public class RestTest {
    private static ResourceManager<String> stringResources = new ResourceManager<>();
    private static AbstractDao<CoinMarketCapListing> ld;

    private RestTest() throws Exception {
        stringResources.putStore("query", Queries.INSTANCE.getQueryStore());

        stringResources.createStore("config");
        stringResources.getStore("config").addResource("DatabaseDriver", "com.mysql.cj.jdbc.Driver");
        stringResources.getStore("config").addResource("DatabaseConnection",
                String.format("jdbc:mysql://%s/cryptocurrency?user=%s&password=%s&useSSL=true&verifyServerCertificate=false",
                        System.getProperty("MYSQL_HOST"),
                        System.getProperty("MYSQL_USER"),
                        System.getProperty("MYSQL_PASS")));

        ld = new ListingsDao<>(CoinMarketCapListing.class)
                .withDriver(stringResources.getStore("config").getResource("DatabaseDriver"))
                .withConnection(stringResources.getStore("config").getResource("DatabaseConnection"));

    }

    /**
     * Um, run it.
     *
     * @throws Exception For problems.
     */
    private void run() throws Exception {
        //dbDeleteListings();
        dbPutListings();
        //dbGetListings();
    }

    /**
     * Makes a call to the "listing" endpoint.
     *
     * @return Listing object.
     * @throws Exception
     */
    private static CoinMarketCapListing callListingApi() throws Exception {
        CoinMarketCapEndpoint<CoinMarketCapListing> endpoint = new CoinMarketCapEndpoint<>(CoinMarketCapListing.class);

        endpoint.init()
                .withMethod(CoinMarketCapEndpoint.LISTINGS_METHOD);

        return (CoinMarketCapListing) endpoint.call();
    }

    /**
     * Makes a call to the ticker api. Limits to 10. Sorts by id.
     *
     * @return Ticker object.
     * @throws Exception
     */
    private CoinMarketCapTickerArray callTickerApi() throws Exception {
        CoinMarketCapEndpoint<CoinMarketCapTickerArray> endpoint =
                new CoinMarketCapEndpoint<>(CoinMarketCapTickerArray.class);

        endpoint.init()
                .withMethod(CoinMarketCapEndpoint.TICKER_METHOD)
                .withParameters(new TickerParameters().setLimit(10).setSort("id"));

        return (CoinMarketCapTickerArray) endpoint.call();
    }

    /**
     * Makes a call to the ticker api. Limits to 10. Sorts by id.
     *
     * @return Ticker object.
     * @throws Exception
     */
    private CoinMarketCapTickerArray callTickerApiWithResource() throws Exception {
        CoinMarketCapEndpoint<CoinMarketCapTickerArray> endpoint =
                new CoinMarketCapEndpoint<>(CoinMarketCapTickerArray.class);

        endpoint.init()
                .withMethod(CoinMarketCapEndpoint.TICKER_METHOD)
                .withResource("1")
                .withParameters(new TickerParameters());

        return (CoinMarketCapTickerArray) endpoint.call();
    }

    /**
     * Saves a listing.
     *
     * @throws Exception
     */
    private void dbPutListings() throws Exception {
        ((ListingsDao<CoinMarketCapListing>) ld).init()
                .withMarketSource("Coin Market Cap")
                .withListings(callListingApi())
                .put().execute();
    }

    private void dbDeleteListings() throws Exception {
        ((ListingsDao<CoinMarketCapListing>) ld).init()
                .withMarketSource("Coin Market Cap")
                .withListings(callListingApi())
                .delete().execute();
    }

    /**
     * Prints out a listing.
     *
     * @throws Exception
     */
    private static void dbGetListings() throws Exception {
        CoinMarketCapListing listings = 
                ((ListingsDao<CoinMarketCapListing>)ld).init()
                .withMarketSource("Coin Market Cap")
                .get();

        System.out.println(listings.getData().size());
        listings.getData().forEach(listing -> {
            System.out.println(listing.getName());
        });
    }

    public static void main(String[] args) {
        try {
            RestTest rt = new RestTest();
            rt.run();
        } catch (Exception exc) {
            System.out.println(exc.getLocalizedMessage());
        }
    }
}
