package com.codejuicer.res;

import com.codejuicer.util.NoSuchResourceException;
import com.codejuicer.util.ResourceStore;

public enum Queries {
    INSTANCE;

    ResourceStore<String> queryStore = null;

    public ResourceStore<String> getQueryStore() {
        // First call, initialize the store.
        if (null == queryStore) {
            queryStore = new ResourceStore<>();
            addListingsQueries();
            addCryptoRatesQueries();
            addMiningQueries();
        }
        
        return queryStore;
    }

    /**
     * Retrieve a query by name.
     * @param queryName The name of the query.
     * @return The SQL string.
     * @throws NoSuchResourceException If the query doesn't exist (by name), then you get this.
     */
    public String getQuery(String queryName) throws NoSuchResourceException {
        return getQueryStore().getResource(queryName);
    }

    /**
     * Add the Listings table queries.
     */
    private void addListingsQueries() {
        queryStore.addResource("Listings.insert",
                "INSERT INTO `cryptocurrency`.`Listings`(MarketSourceId, Listings) " +
                        "  SELECT MarketSourceId, ? FROM MarketSources " +
                        "  WHERE MarketSourceName = ? " +
                        "   ON DUPLICATE KEY UPDATE Listings=?");

        queryStore.addResource("Listings.delete", "DELETE FROM `cryptocurrency`.`Listings` " +
                "WHERE MarketSourceId IN (" +
                " SELECT MarketSourceId FROM MarketSources " +
                " WHERE MarketSourceName = ?)");

        queryStore.addResource("Listings.select", "SELECT listings FROM `cryptocurrency`.`Listings` " +
                "WHERE MarketSourceId IN (" +
                " SELECT MarketSourceId FROM MarketSources " +
                " WHERE MarketSourceName = ?)");        
    }

    /**
     * Add the CryptoRates table queries.
     */
    private void addCryptoRatesQueries() {
        queryStore.addResource("CryptoRates.insert",
                "INSERT INTO `cryptocurrency`.`CryptoRates`(MarketSourceId, Rates) " +
                        "  SELECT MarketSourceId, ? FROM MarketSources " +
                        "  WHERE MarketSourceName = ? ON DUPLICATE KEY UPDATE Rates = ?");

        queryStore.addResource("CryptoRates.delete",
                "DELETE FROM `cryptocurrency`.`CryptoRates` " +
                        "WHERE MarketSourceId IN (" +
                        " SELECT MarketSourceId FROM MarketSources " +
                        " WHERE CryptoRateId = ?)");

        queryStore.addResource("CryptoRates.select",
                "SELECT Rates FROM `cryptocurrency`.`CryptoRates` " +
                        "WHERE MarketSourceId IN (" +
                        " SELECT MarketSourceId FROM MarketSources " +
                        " WHERE MarketSourceName = ?)" +
                        " AND CryptocurrencyName = ?" +
                        " ORDER BY LastUpdated DESC");        
    }

    private void addMiningQueries() {
        queryStore.addResource("Mining.insert",
                "INSERT IGNORE INTO `cryptocurrency`.`Mining`(MarketSourceId, CryptocurrencyId, Reward) " +
                        "  SELECT MarketSourceId, ? FROM MarketSources " +
                        "  WHERE MarketSourceName = ? ON DUPLICATE KEY UPDATE Rates = ?");

        queryStore.addResource("Mining.delete",
                "DELETE FROM `cryptocurrency`.`Mining` " +
                        "WHERE MarketSourceId IN (" +
                        " SELECT MarketSourceId FROM MarketSources " +
                        " WHERE CryptoRateId = ?)");

        queryStore.addResource("Mining.select",
                "SELECT Rates FROM `cryptocurrency`.`Mining` " +
                        "WHERE MarketSourceId IN (" +
                        " SELECT MarketSourceId FROM MarketSources " +
                        " WHERE MarketSourceName = ?)" +
                        " AND CryptocurrencyName = ?" +
                        " ORDER BY LastUpdated DESC");
    }    
    
}
