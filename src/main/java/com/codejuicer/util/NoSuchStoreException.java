package com.codejuicer.util;

public class NoSuchStoreException extends Exception {
    
    public NoSuchStoreException(String storeName) {
        super(String.format("A resource store named %s does not exist.", storeName));
    }
    
}
