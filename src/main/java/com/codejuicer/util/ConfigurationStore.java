package com.codejuicer.util;

import java.util.HashMap;

/**
 * Utility for configuration data storage and retrieval.
 */
public class ConfigurationStore {
    private static ConfigurationStore instance;
    private HashMap<String, HashMap> store;

    public static String STRING_STORE ="string";
    public static String QUERY_STORE ="query";

    // TODO: make this a secure store with real initilazation from outside sources.
    private ConfigurationStore() {
        store = new HashMap<>();
        HashMap<String, String> stringStore = new HashMap<>();
        
        /*----------------------------------------------------
          - Database configuration
          ----------------------------------------------------*/
        stringStore.put("DatabaseDriver", "com.mysql.cj.jdbc.Driver");
        stringStore.put("DatabaseConnection", 
                String.format("jdbc:mysql://%s/cryptocurrency?user=%s&password=%s&useSSL=true&verifyServerCertificate=false",
                        System.getProperty("MYSQL_HOST"), 
                        System.getProperty("MYSQL_USER"), 
                        System.getProperty("MYSQL_PASS")));
        
        store.put(STRING_STORE, stringStore);
        setQueryStore();
    }

    /**
     * This be a singleton.
     *
     * @return Instantiated singleton.
     */
    public static ConfigurationStore getInstance() {
        if (instance == null) {
            synchronized (ConfigurationStore.class) {
                if (instance == null) {
                    instance = new ConfigurationStore();
                }
            }
        }

        return instance;
    }

    /**
     * A collection of queries.
     */
    private void setQueryStore() {
        // TODO: Read this from a file somewhere instead.
        HashMap<String, String> queryStore = new HashMap<>();
        
        /* ---------------------------- LISTINGS TABLE ---------------------------- */
        queryStore.put("Listings.insert", 
                "INSERT INTO `cryptocurrency`.`Listings`(MarketSourceId, Listings) " +
                "  SELECT MarketSourceId, ? FROM MarketSources " +
                "  WHERE MarketSourceName = ? " +
                "   ON DUPLICATE KEY UPDATE Listings=?");
        
        queryStore.put("Listings.delete", "DELETE FROM `cryptocurrency`.`Listings` " +
                "WHERE MarketSourceId IN (" +
                " SELECT MarketSourceId FROM MarketSources " +
                " WHERE MarketSourceName = ?)");
        
        queryStore.put("Listings.select", "SELECT listings FROM `cryptocurrency`.`Listings` " +
                "WHERE MarketSourceId IN (" +
                " SELECT MarketSourceId FROM MarketSources " +
                " WHERE MarketSourceName = ?)");
        
        /* ---------------------------- CRYPTORATES TABLE ---------------------------- */
        queryStore.put("CryptoRates.insert",
                "INSERT INTO `cryptocurrency`.`CryptoRates`(MarketSourceId, Rates) " +
                        "  SELECT MarketSourceId, ? FROM MarketSources " +
                        "  WHERE MarketSourceName = ? ON DUPLICATE KEY UPDATE Rates = ?");

        queryStore.put("CryptoRates.delete",
                "DELETE FROM `cryptocurrency`.`CryptoRates` " +
                        "WHERE MarketSourceId IN (" +
                        " SELECT MarketSourceId FROM MarketSources " +
                        " WHERE CryptoRateId = ?)");
        
        store.put(QUERY_STORE, queryStore);
    }

    /**
     * Gets a String value from a configuration store
     * @param storeName Name of the store to get the value from.
     * @param keyName Key the value is stored under.
     * @param storeType Class of the value stored in the... store. String in this case.
     * @return The value as a string.
     * @throws Exception Class cast, missing store, or missing key exceptions.
     */
    public String get(String storeName, String keyName, Class<String> storeType) throws Exception {
        if (store.containsKey(storeName)) {
            if (store.get(storeName).containsKey(keyName))
                return (String) store.get(storeName).get(keyName);
            else
                throw new Exception(String.format("A key named %s was not found in the String store named %s.", keyName, storeName));
        }

        throw new Exception(String.format("A String store named %s was not found.", storeName));
    }
}
