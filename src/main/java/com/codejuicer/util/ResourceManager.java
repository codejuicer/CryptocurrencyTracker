package com.codejuicer.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Manages resources of a specific Class. I might make this a singleton.
 * @param <T> Type of data being managed. Hint: resource value data type.
 */
public class ResourceManager<T> {
    private Map<String, ResourceStore<T>> resourceStores;
    
    public ResourceManager() {
        resourceStores = new HashMap<>();
    }

    /**
     * Creates a new resource store with the given name.
     * @param storeName Name of the resource store.
     * @throws ResourceStoreAlreadyExistsException Store with the name has already been created.
     */
    public void createStore(String storeName) throws ResourceStoreAlreadyExistsException {
        if(resourceStores.containsKey(storeName))
            throw new ResourceStoreAlreadyExistsException(storeName);
        
        resourceStores.put(storeName, new ResourceStore<T>());
    }

    /**
     * Removes a resource store. Why would you want to do that? I don't know.
     * @param storeName Name of the store to remove.
     * @throws NoSuchStoreException Pfft. You can't remove a store that doesn't exist.
     */
    public void removeStore(String storeName) throws NoSuchStoreException {
        if(!resourceStores.containsKey(storeName))
            throw new NoSuchStoreException(storeName);
        
        resourceStores.remove(storeName);
    }
    
    public void putStore(String storeName, ResourceStore<T> store) throws ResourceStoreAlreadyExistsException {
        if(resourceStores.containsKey(storeName))
            throw new ResourceStoreAlreadyExistsException(storeName);
        
        resourceStores.put(storeName, store);
    }

    /**
     * Get a store. By name.
     * @param storeName Name of the store.
     * @return The store--if it exists.
     * @throws NoSuchStoreException You didn't spell it right. There is no store.
     */
    public ResourceStore<T> getStore(String storeName) throws NoSuchStoreException {
        if(!resourceStores.containsKey(storeName))
            throw new NoSuchStoreException(storeName);
        
        return resourceStores.get(storeName);
    }
}
