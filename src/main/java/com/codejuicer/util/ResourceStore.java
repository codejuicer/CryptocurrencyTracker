package com.codejuicer.util;

import java.util.HashMap;

/**
 * A simple object to store resources in by a key (resource name). Uses a HashMap behind the scenes.
 * @param <T> Class type used for the resource's values.
 */
public class ResourceStore<T> {
    private HashMap<String, T> store;
    
    public ResourceStore() {
        store = new HashMap<>();
    }

    /**
     * Add a resource to the store.
     * @param resourceName Unique identifying name.
     * @param resourceValue The value of the resource.
     */
    public void addResource(String resourceName, T resourceValue) {
        this.store.put(resourceName, resourceValue);
    }

    /**
     * Retrieves a resource value by name.
     * @param resourceName
     * @return
     * @throws NoSuchResourceException
     */
    public T getResource(String resourceName) throws NoSuchResourceException {
        if(store.containsKey(resourceName))
            return store.get(resourceName);
        
        throw new NoSuchResourceException(resourceName);
    }

    /**
     * Get all the possible resources represented in the store.
     * @return Array of keys.
     */
    public String[] getResourceNames() {
        return store.keySet().toArray(new String[0]);
    }
    
}
