package com.codejuicer.util;

public class ResourceStoreAlreadyExistsException extends Exception {
    
    public ResourceStoreAlreadyExistsException(String storeName) {
        super(String.format("A resource store named %s already exists.", storeName));
    }
    
}
