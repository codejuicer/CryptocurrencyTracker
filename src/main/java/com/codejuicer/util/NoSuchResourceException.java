package com.codejuicer.util;

public class NoSuchResourceException extends Exception {
    
    public NoSuchResourceException(String resourceName) {
        super(String.format("Resource named %s could not be found.", resourceName));
    }
    
}
