package com.codejuicer.rest;

import java.util.HashMap;

public abstract class AbstractEndpointParameters {
    protected HashMap<String, String> parameters;
    
    protected AbstractEndpointParameters() {
        parameters = new HashMap<>();
    }
    
    public HashMap<String, String> getParameters() {
        return parameters;
    }
}
