package com.codejuicer.rest;

import com.google.api.client.http.HttpResponse;

public interface IEndpoint<T> {
    IEndpoint init();
    
    T call() throws Exception;
    
    IEndpoint withParameters(AbstractEndpointParameters parameters) throws Exception;

    IEndpoint withMethod(String methodName) throws Exception;
    
    IEndpoint withResource(String resourceId) throws Exception;
}
