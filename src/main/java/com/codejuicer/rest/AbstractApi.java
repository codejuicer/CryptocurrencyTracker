package com.codejuicer.rest;

import com.codejuicer.rest.coinmarketcap.CoinMarketCapEndpoint;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.gson.GsonFactory;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

/**
 * Abstract API endpoint interaction class.
 * @param <T> Class for the GSON-based response object.
 */
public class AbstractApi<T> implements IEndpoint {
    private final String endpointUrl;
    private static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
    private static final JsonFactory JSON_FACTORY = new GsonFactory();
    private static final Gson gson = new Gson();

    /** Parameters for the endpoint method */
    private AbstractEndpointParameters parameters = null;
    /** The endpoint method */
    private String methodName = null;
    /** A resource ID for use where appropriate */
    protected String resourceId = null;
    private final Class<T> responseClassType;
    
    protected AbstractApi() {
        this.endpointUrl = null;
        this.responseClassType = null;
    }
    
    protected AbstractApi(String endpointUrl, Class<T> responseClassType) {
        this.endpointUrl = endpointUrl;
        this.responseClassType = responseClassType;
    }

    public String getEndpointUrl() {
        return endpointUrl;
    }

    @Override
    public IEndpoint init() {
        parameters = null;
        methodName = null;
        resourceId = null;
        return this;
    }

    /**
     * Calls an endpont and converts the response to a GSON-based object.
     * @return 
     * @throws Exception Could be a JSON parsing exception, or a HTTP request exception of sorts.
     */
    @Override
    public T call() throws Exception {
        if (null == this.methodName) throw new Exception("Method name expected, none was given. Call 'withMethod' first.");
        HttpRequestFactory requestFactory = HTTP_TRANSPORT.createRequestFactory(
                request -> request.setParser(new JsonObjectParser(JSON_FACTORY)));

        /* Build the endpoint URL */
        CoinMarketCapEndpoint.CoinMarketcapUrl url = new CoinMarketCapEndpoint.CoinMarketcapUrl(this.endpointUrl);
        url.appendRawPath(this.methodName + "/");
        
        /* Resource reference */
        if(null != resourceId) {
            url.appendRawPath(resourceId + "/");
        }
        
        /* Add parameters */
        if(null != parameters) {
            List<String> paramList = new ArrayList<>();
            parameters.getParameters().forEach(url::putIfAbsent);
        }
        
        /* Make the request */
        HttpRequest request = requestFactory.buildGetRequest(url);
        request.setThrowExceptionOnExecuteError(false);
                
        // Just return nothing on a 404.
        // TODO: Do something better.
        HttpResponse resp = request.execute();
        if(resp.getStatusCode() == 404)
            return null;
        
        // The request comes back as a string. There's probably a better way, but I can't get the JSON_FACTORY thing to work.
        final String jsonValue = resp.parseAsString();
        resp.disconnect();

        return gson.fromJson(jsonValue, responseClassType);
    }

    @Override
    public IEndpoint withParameters(AbstractEndpointParameters parameters) {
        this.parameters = parameters;
        return this;
    }

    @Override
    public IEndpoint withMethod(String methodName) throws Exception {
        this.methodName = methodName;
        return this;
    }

    @Override
    public IEndpoint withResource(String resourceId) {
        this.resourceId = resourceId;
        return this;
    }
}
