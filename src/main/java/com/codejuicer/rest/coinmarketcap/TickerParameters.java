package com.codejuicer.rest.coinmarketcap;

import com.codejuicer.rest.AbstractEndpointParameters;

import java.util.HashMap;

public class TickerParameters extends AbstractEndpointParameters {
    private Integer start = null;
    private Integer limit = 100;
    private String sort = null;
    private String structure = "array";
    private String convert = "BTC";
    
    public TickerParameters() {
        super();
    }

    @Override
    public HashMap<String, String> getParameters() {
        if(null != start) parameters.put("start", start.toString());
        if(null != limit) parameters.put("limit", limit.toString());
        if(null != sort) parameters.put("sort", sort);
        if(null != structure) parameters.put("structure", structure);
        if(null != convert) parameters.put("convert", convert);
        
        return super.getParameters();
    }

    public Integer getStart() {
        return start;
    }

    public TickerParameters setStart(Integer start) {
        this.start = start;
        return this;
    }

    public Integer getLimit() {
        return limit;
    }

    public TickerParameters setLimit(Integer limit) {
        this.limit = limit;
        return this;
    }

    public String getSort() {
        return sort;
    }

    public TickerParameters setSort(String sort) {
        this.sort = sort;
        return this;
    }

    public String getStructure() {
        return structure;
    }

    public TickerParameters setStructure(String structure) {
        this.structure = structure;
        return this;
    }

    public String getConvert() {
        return convert;
    }

    public TickerParameters setConvert(String convert) {
        this.convert = convert;
        return this;
    }
}
