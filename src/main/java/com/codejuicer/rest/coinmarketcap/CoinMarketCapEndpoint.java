package com.codejuicer.rest.coinmarketcap;

import com.codejuicer.rest.AbstractApi;
import com.codejuicer.rest.IEndpoint;
import com.google.api.client.http.GenericUrl;

import com.google.api.client.util.Key;

import javax.management.InvalidAttributeValueException;
import java.util.Arrays;
import java.util.List;

/**
 * Interacts with a Coin Market Cap endpoint.
 *
 * @param <T> GSON based class type.
 */
public class CoinMarketCapEndpoint<T> extends AbstractApi {

    public static final String LISTINGS_METHOD = "listings";
    public static final String TICKER_METHOD = "ticker";
    public static final String GLOBAL_METHOD = "global";
    
    /**
     * Valid endpoint methods.
     */
    private static final List<String> endpointMethods = Arrays.asList(LISTINGS_METHOD,
            TICKER_METHOD,
            GLOBAL_METHOD);
    

    private CoinMarketCapEndpoint() {
        super();
    }

    /**
     * Creates a new endpoint interaction object with the given response class type.
     *
     * @param responseClassType Endpoint response GSON type.
     */
    public CoinMarketCapEndpoint(Class<T> responseClassType) {
        super("https://api.coinmarketcap.com/v2/", responseClassType);
    }

    public static class CoinMarketcapUrl extends GenericUrl {
        public CoinMarketcapUrl(String encodedUrl) {
            super(encodedUrl);
        }

        @Key
        public String fields;
    }    
    @Override
    public IEndpoint withMethod(String methodName) throws Exception {
        if (!endpointMethods.contains(methodName)) {
            throw new InvalidAttributeValueException("Invalid Coin Market Cap method: " + methodName);
        }
        
        return super.withMethod(methodName);
    }

}
