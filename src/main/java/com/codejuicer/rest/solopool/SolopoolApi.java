package com.codejuicer.rest.solopool;

import com.codejuicer.rest.AbstractApi;
import com.codejuicer.rest.AbstractEndpointParameters;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 * Implementation of the API endpoint for Solopool.
 * @param <T> A GSON based class for the API result. This should be SolopoolStats.
 */
public class SolopoolApi<T> extends AbstractApi {
    private final String cryptocurrency;
    
    public SolopoolApi(final Class<T> responseClassType, final String cryptocurrency) throws Exception {
        super(String.format("https://%s.solopool.org/api/", cryptocurrency), responseClassType);
        this.cryptocurrency = cryptocurrency;
        super.withMethod("accounts");
    }

    public String getCryptocurrency() {
        return cryptocurrency;
    }

    /**
     * Not supported for Solopool.
     * @param parameters Useless parameters.
     * @return It wont. It will throw an exception instead.
     * @throws NotImplementedException Because it isn't supported.
     */
    @Override
    public SolopoolApi withParameters(AbstractEndpointParameters parameters) throws NotImplementedException {
        throw new NotImplementedException();
    }

    /**
     * Not supported for Solopool. There is only one method: accounts. And that is set at initialization.
     * @param methodName Useless method name. It won't be set.
     * @return It wont. It will throw an exception instead.
     * @throws NotImplementedException Because it isn't supported.
     */    
    @Override
    public SolopoolApi withMethod(String methodName) throws NotImplementedException {
        throw new NotImplementedException();
    }

    /**
     * Set the account hash here.
     * @param resourceId Account hash.
     * @return This instance.
     */
    @Override
    public SolopoolApi withResource(String resourceId) {
        super.withResource(resourceId);
        return this;
    }

    /**
     * Calls the Solopool API endpoint. The account hash must have been set using withResource first.
     * @return API Json result.
     * @throws Exception Missing resourceId, or some other API issue.
     */
    @Override
    public T call() throws Exception {
        if(null == this.resourceId) {
            throw new Exception("resourceId not found: Solopool requires the account hash as the resource. Call withResource first.");
        }
        
        return (T)super.call();
    }
}
