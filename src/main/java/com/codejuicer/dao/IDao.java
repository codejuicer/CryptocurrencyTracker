package com.codejuicer.dao;

import java.sql.SQLException;
import java.util.List;

/**
 * A set of normal methods for DAO objects.
 */
interface IDao<T> {

    IDao<T> init();

    /**
     * Set s the data driver.
     * @param dbDriver Driver class string.
     */
    AbstractDao<T> withDriver(String dbDriver);

    /**
     * Sets the data source connection string.
     * @param connectionString A valid connection string.
     * @return Instance of the object for chaining.
     */
    AbstractDao<T> withConnection(String connectionString);    
    
    /**
     * Fetch a single record.
     * @return A result.
     * @throws Exception Some kind of SQL exception.
     */
    T get() throws Exception;

    /**
     * Retrieve all records from a query.
     * @return A list of records.
     * @throws Exception
     */
    List<T> getAll() throws Exception;
    
    /**
     * Inserts a record.
     * @return Affected record count.
     * @throws Exception For missing parameters or SQL exceptions.
     */
    IDao<T> put() throws Exception;

    /**
     * For batched inserts, this is the starting point.
     * @return
     * @throws Exception
     */
    IDao<T> asBatch() throws Exception;

    /**
     * Add a record to the batch.
     * @return Instance of the object.
     * @throws Exception Couldn't add it for some reason.
     */
    IDao<T> addToBatch() throws Exception;

    /**
     * Add multiple records to the batch.
     * @param batchObjects Happy list of objects for the records.
     * @return Instance of the object.
     * @throws SQLException Couldn't add something. Only returns the first error if multiple happened. Yeah, lazy. But simple.
     */
    IDao<T> addToBatch(List<T> batchObjects) throws Exception;

    /**
     * Delete a record.
     * @return Affected record count.
     * @throws Exception For missing parameters or SQL exceptions.
     */
    IDao<T> delete() throws Exception;

    /**
     * For delete and insert statements, this should be called to make it happen.
     * @return Count of records affected.
     * @throws Exception SQL problems and such.
     */
    int execute() throws Exception;
}
