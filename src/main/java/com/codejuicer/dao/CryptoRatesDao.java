package com.codejuicer.dao;

import com.codejuicer.res.Queries;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.sql.JDBCType;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/**
 * DAO for the cryptocurrency Listings table.
 */
public class CryptoRatesDao<T> extends AbstractDao<T> {
    private String marketSourceName = null;
    private T rates = null;
    private final Class<T> typeParameterClass;

    private final String INSERT_QUERY;
    private final String SELECT_QUERY;
    private final String DELETE_QUERY;

    private static final String TABLE_NAME = "CryptoRates";

    /**
     * Standard exception for a missing market source name.
     */
    private static final Exception MARKET_SOURCE_MISSING = new Exception("Market source name expected. Use 'withMarketSource' first.");

    /**
     * Standard exception for a missing rates object.
     */
    private static final Exception RATES_MISSING = new Exception("No rates object provided. Use 'withRates' first.");


    /**
     * Create a new object.
     * @param listingsTypeClass The template class.
     * @throws Exception Any problems. Like, accessing the query store.
     */
    public CryptoRatesDao(Class<T> listingsTypeClass) throws Exception {
        super();
        this.typeParameterClass = listingsTypeClass;

        INSERT_QUERY = Queries.INSTANCE.getQuery(TABLE_NAME + ".insert");
        SELECT_QUERY = Queries.INSTANCE.getQuery(TABLE_NAME + ".select");
        DELETE_QUERY = Queries.INSTANCE.getQuery(TABLE_NAME + ".delete");
    }

    /**
     * Entrypoint for the builder.
     *
     * @return Instance of the object.
     */
    public CryptoRatesDao<T> init() {
        marketSourceName = null;
        rates = null;
        return this;
    }

    /**
     * Kinda self explanatory. It sets the market source name.
     * @param marketSourceName The name.
     * @return This.
     */
    public CryptoRatesDao<T> withMarketSource(String marketSourceName) {
        this.marketSourceName = marketSourceName;
        return this;
    }

    /**
     * A rates object to delete. Must be a GSON object.
     *
     * @param rates The GSON rates object.
     * @return Instance of this object.
     */
    public CryptoRatesDao<T> withRates(T rates) {
        this.rates = rates;
        return this;
    }

    /**
     * Fetches 0 or 1 rates record. Even if there are multiple results, this always returns the first one.
     *
     * @return A rate object.
     * @throws Exception
     */
    public T get() throws Exception {
        if (null == marketSourceName) {
            throw MARKET_SOURCE_MISSING;
        }

        /* Zero or one row expected to be retrieved. */
        try (ResultSet rs = connection.newQuery(SELECT_QUERY).prepare()
                .setQueryParameter(1, marketSourceName, JDBCType.VARCHAR)
                .execute()) {
            if (rs.first()) {
                Gson gson = new Gson();
                return gson.fromJson(rs.getString(1), this.typeParameterClass);
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw e;
        } catch (JsonSyntaxException e) {
            throw e;
        }
    }

    /**
     * Return a bunch of rates.
     * @return Right now... nothing.
     */
    public List<T> getAll() {
        throw new NotImplementedException();
//        if (null == marketSourceName) {
//            throw MARKET_SOURCE_MISSING;
//        }
//        
//        PreparedStatement preparedStatement = connection.newQuery(SELECT_QUERY).prepare();
//        preparedStatement.setString(1, marketSourceName);
//        return null;    
    }

    /**
     * Start a batch.
     * @return Instance of this object.
     * @throws Exception Missing market source, rates or SQL exception.
     */
    @Override
    public CryptoRatesDao<T> asBatch() throws Exception {
        if (null == marketSourceName) {
            throw MARKET_SOURCE_MISSING;
        }

        super.asBatch();

        // Create a new prepared statement.
        connection.newQuery(INSERT_QUERY).prepare();

        return this;
    }    

    /**
     * Inserts a record into the Listings table.
     * The market source name and rates must be set before calling. An Exception will be thrown if it isn't.
     *
     * @return Count of records affected.
     * @throws Exception Missing values or bad SQL happenings.
     */
    public CryptoRatesDao<T> put() throws Exception {
        if (null == marketSourceName) {
            throw MARKET_SOURCE_MISSING;
        }
        if (null == rates) {
            throw RATES_MISSING;
        }
        
         connection.newQuery(INSERT_QUERY).prepare();
         setParameterValues();
         
         return this;
    }

    /**
     * Add a rate to the batch.
     * @return This.
     * @throws SQLException Boo! Failure in SQL land.
     */
    @Override
    public CryptoRatesDao<T> addToBatch() throws Exception {
        if (null == rates) {
            throw RATES_MISSING;
        }
        
        String listingsStr = new Gson().toJson(rates);

        connection.setQueryParameter(1, listingsStr, JDBCType.VARCHAR)
                .setQueryParameter(2, marketSourceName, JDBCType.VARCHAR)
                .setQueryParameter(3, listingsStr, JDBCType.VARCHAR)
                .appendToBatch();

        return this;
    }

    /**
     * Add multiple rates.
     * @param batchObjects Happy list of objects for the records.
     * @return This thing.
     * @throws SQLException Ooops, something failed at least once. You get the first one.
     */
    @Override
    public CryptoRatesDao<T> addToBatch(List<T> batchObjects) throws Exception {
        List<Exception> exceptions = new ArrayList<>();

        batchObjects.forEach(batchObject -> {
            try {
                withRates(batchObject).addToBatch();
            } catch (Exception e) {
                exceptions.add(e);
            }
        });

        if(exceptions.size() > 0)
            throw exceptions.get(0);

        return this;
    }

    /**
     * Sets the parameter values.
     * @throws SQLException Didn't work.
     */
    private void setParameterValues() throws SQLException {
        String ratesStr = new Gson().toJson(rates);

        connection.setQueryParameter(1, ratesStr, JDBCType.VARCHAR)
                .setQueryParameter(2, marketSourceName, JDBCType.VARCHAR)
                .setQueryParameter(3, ratesStr, JDBCType.VARCHAR);        
    }

    /**
     * Deletes the rates for a market.
     * The market source name must be set before calling delete. MissingFieldException will be thrown if it isn't.
     *
     * @return Count of affected records.
     * @throws Exception
     */
    public CryptoRatesDao<T> delete() throws Exception {
        if (null == marketSourceName) {
            throw MARKET_SOURCE_MISSING;
        }
        
        connection.newQuery(DELETE_QUERY).prepare()
                .setQueryParameter(1, marketSourceName, JDBCType.VARCHAR);
        
        return this;
    }
    
}
