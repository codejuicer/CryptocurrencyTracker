package com.codejuicer.dao;

public abstract class AbstractDao<T> implements IDao<T> {
    protected SqlConnection connection = null;
    protected boolean asBatch = false;
    
    AbstractDao() {
        connection = new SqlConnection();
    }
    
    public AbstractDao<T> init() {
        this.asBatch = false;
        return this;
    }
    
    @Override
    public AbstractDao<T> asBatch() throws Exception {
        this.asBatch = true;
        return this;
    }

    @Override
    public AbstractDao<T> withDriver(String dbDriver) {
        connection.setDriver(dbDriver);
        return this;
    }

    @Override
    public AbstractDao<T> withConnection(String connectionString) {
        connection.setConnectionString(connectionString);
        return this;
    }

    /**
     * For
     * @return
     * @throws Exception
     */
    @Override
    public int execute() throws Exception {
        if(asBatch)
            return connection.executeBatch();
        
        return connection.update();
    }    
    
}
