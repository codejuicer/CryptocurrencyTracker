package com.codejuicer.dao;

import java.util.List;

public class MiningDao<T> extends AbstractDao<T> {

    @Override
    public T get() throws Exception {
        return null;
    }

    @Override
    public List<T> getAll() throws Exception {
        return null;
    }

    @Override
    public IDao<T> put() throws Exception {
        return null;
    }

    @Override
    public IDao<T> addToBatch() throws Exception {
        return null;
    }

    @Override
    public IDao<T> addToBatch(List<T> batchObjects) throws Exception {
        return null;
    }

    @Override
    public IDao<T> delete() throws Exception {
        return null;
    }
}
