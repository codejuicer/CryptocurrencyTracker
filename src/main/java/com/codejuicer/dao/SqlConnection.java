package com.codejuicer.dao;

import java.sql.*;
import java.util.Arrays;

class SqlConnection {
    private Connection conn = null;
    private String query = null;
    private String connectionString;
    private String driver = null;
    private PreparedStatement preparedStatement = null;

    /**
     * Sets the data driver and connection string.
     * @throws Exception For invalid driver (class name) or class instantiation exceptions.
     */
    public SqlConnection() {
        super();
    }
    

    public final SqlConnection setDriver(final String driver) {
        this.driver = driver;
        return this;
    }

    public final SqlConnection setConnectionString(final String connectionString) {
        this.connectionString = connectionString;
        return this;
    }

    /**
     * Starting point for queries. Clears any prior connection and statements to start fresh.
     * @param query The query to run. This gets converted to a prepared statement.
     * @return Instance of the object for chaining.
     * @throws SQLException In the strange event a SQL exception occurs with the connection.
     */
    SqlConnection newQuery(String query) throws Exception {
        this.query = query;
        this.preparedStatement = null;
        
        if(null != conn && !conn.isClosed()) {
            conn.close();
        }
        Class.forName(driver);
        conn = DriverManager.getConnection(connectionString);
        
        return this;
    }
    
    
    SqlConnection appendToBatch() throws SQLException {
        preparedStatement.addBatch();
        return this;
    }
    
    /**
     * Creates the prepared statement.
     * @return Instance of the object.
     * @throws SQLException If something goes wrong with the DB connection.
     */
    SqlConnection prepare() throws SQLException {
        preparedStatement = conn.prepareStatement(this.query);
        return this;
    }

    /**
     * Add a parameter to the prepared statement.
     * @param parameterIndex Index of the parameter (1 based).
     * @param value The actual parameter value.
     * @param sqlDataType Data type to use.
     * @return Instance of the object.
     * @throws SQLException Either you don't have a valid prepared statement, or the data type is wrong.
     */
    SqlConnection setQueryParameter(int parameterIndex, Object value, SQLType sqlDataType) throws SQLException {
        preparedStatement.setObject(parameterIndex, value, sqlDataType);
        return this;
    }

    /**
     * Executes a prepared statement batch.
     * @return Count of records affected.
     * @throws Exception If anything goes awry, this will happen.
     */
    int executeBatch() throws Exception {
        if(null == preparedStatement)
            throw new Exception("No prepared statement for batch.");
        
        try {
            int[] recordsAffected = preparedStatement.executeBatch();

            if (!conn.getAutoCommit())
                conn.commit();
            
            return Arrays.stream(recordsAffected).sum();
            
        } catch(SQLException sqlExc) {
            throw sqlExc;
        }
        finally {
            if (!conn.isClosed()) {
                conn.close();
            }
        }
    }

    /**
     * Executes the "update" type prepared statement.
     *
     * @return Records affected.
     * @throws SQLException Something bad happened.
     */
    int update() throws SQLException {
        try {
            if(null == preparedStatement)
                preparedStatement = conn.prepareStatement(query);
            
            int recordsAffected = preparedStatement.executeUpdate();
            
            if(!conn.getAutoCommit())
                conn.commit();
            
            return recordsAffected;
            
        } catch (SQLException se) {
            throw se;
        } finally {
            if (!conn.isClosed()) {
                conn.close();
            }
        }
    }

    /**
     * Runs a query.
     * @return
     * @throws SQLException
     */
    ResultSet execute() throws SQLException {
        try {
            if(null == preparedStatement)
                preparedStatement = conn.prepareStatement(query);
            
            return preparedStatement.executeQuery();
        } catch (SQLException se) {
            throw se;
        }
    }
}
