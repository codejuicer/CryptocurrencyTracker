package com.codejuicer.dao;

import com.codejuicer.res.Queries;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.sql.JDBCType;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/**
 * DAO for the cryptocurrency Listings table.
 */
public class ListingsDao<T> extends AbstractDao<T> {
    private String marketSourceName = null;
    private T listings = null;
    private final Class<T> typeParameterClass;

    private final String INSERT_QUERY;
    private final String SELECT_QUERY;
    private final String DELETE_QUERY;

    /**
     * Standard exception for a missing market source name.
     */
    private static final Exception MARKET_SOURCE_MISSING = new Exception("Market source name expected. Use 'withMarketSource' first.");

    /**
     * Standard exception for a missing listings object.
     */
    private static final Exception LISTING_MISSING = new Exception("No listings object provided. Use 'withListings' first.");


    public ListingsDao(Class<T> listingsTypeClass) throws Exception {
        super();
        this.typeParameterClass = listingsTypeClass;

        INSERT_QUERY = Queries.INSTANCE.getQuery("Listings.insert");
        SELECT_QUERY = Queries.INSTANCE.getQuery("Listings.select");
        DELETE_QUERY = Queries.INSTANCE.getQuery("Listings.delete");
    }

    /**
     * Entrypoint for the builder.
     *
     * @return Instance of the object.
     */
    public ListingsDao<T> init() {
        super.init();
        marketSourceName = null;
        listings = null;
        return this;
    }

    public ListingsDao<T> withMarketSource(String marketSourceName) {
        this.marketSourceName = marketSourceName;
        return this;
    }

    /**
     * A listing object to delete. Must be a GSON object.
     *
     * @param listings The GSON listing object.
     * @return Instance of this object.
     */
    public ListingsDao<T> withListings(T listings) {
        this.listings = listings;
        return this;
    }

    /**
     * Fetch a single Listings record. the market source name must be set first.
     *
     * @return 0 or 1 listings record.
     * @throws Exception Missing market source name, sql exceptions, or JSON parse exceptions.
     */
    @Override
    public T get() throws Exception {
        if (null == marketSourceName) {
            throw MARKET_SOURCE_MISSING;
        }

        /* Zero or one row expected to be retrieved. */
        try (ResultSet rs = connection.newQuery(SELECT_QUERY)
                .prepare()
                .setQueryParameter(1, marketSourceName, JDBCType.VARCHAR).execute()) {

            if (rs.first()) {
                Gson gson = new Gson();
                // Only one column is fetched. The listing column.
                return gson.fromJson(rs.getString(1), this.typeParameterClass);
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw e;
        } catch (JsonSyntaxException e) {
            //TODO: Might want to do something different here.
            System.err.println(e.getLocalizedMessage());
            throw e;
        }
    }

    /**
     * Only one record is ever retreived, so this just calls get() and wraps the result in a List.
     *
     * @return List contiaining 0 or 1 record.
     * @throws Exception See get() doc comment.
     */
    @Override
    public List<T> getAll() throws Exception {
        List<T> records = new ArrayList<>();
        records.add(this.get());
        return records;
    }

    /**
     * Adds a listing object to the batch for insert.
     * @return Instance of the object.
     * @throws SQLException If the prepared statment hasn't been initialized (asBatch), this will happen. Or any other SQL exception for that matter.
     */
    @Override
    public ListingsDao<T> addToBatch() throws Exception {
        if (null == listings) {
            throw LISTING_MISSING;
        }
        
        String listingsStr = new Gson().toJson(listings);
        
        connection.setQueryParameter(1, listingsStr, JDBCType.VARCHAR)
                .setQueryParameter(2, marketSourceName, JDBCType.VARCHAR)
                .setQueryParameter(3, listingsStr, JDBCType.VARCHAR)
                .appendToBatch();
        
        return this;
    }

    /**
     * Add multiple listings.
     * @param batchObjects Happy list of objects for the records.
     * @return Instance of the object.
     * @throws SQLException Yup, SQL exceptions.
     */
    @Override
    public ListingsDao<T> addToBatch(List<T> batchObjects) throws Exception {
        List<Exception> exceptions = new ArrayList<>();
        
        batchObjects.forEach(batchObject -> {
            try {
                withListings(batchObject).addToBatch();
            } catch (Exception e) {
                exceptions.add(e);
            }
        });
        
        if(exceptions.size() > 0)
            throw exceptions.get(0);
        
        return this;
    }


    @Override
    public ListingsDao<T> asBatch() throws Exception {
        if (null == marketSourceName) {
            throw MARKET_SOURCE_MISSING;
        }

        super.asBatch();
        
        // Create a new prepared statement.
        connection.newQuery(INSERT_QUERY).prepare();      
        
        return this;
    }

    /**
     * Inserts a record into the Listings table.
     * The market source name and listings must be set before calling. A MissingFieldException will be thrown if it isn't.
     *
     * @return Count of records affected.
     * @throws Exception MissingFieldException and SQLException may be thrown.
     */
    @Override
    public ListingsDao<T> put() throws Exception {
        if (null == marketSourceName) {
            throw MARKET_SOURCE_MISSING;
        }
        if (null == listings) {
            throw LISTING_MISSING;
        }

        String listingsStr = new Gson().toJson(listings);

        connection.newQuery(INSERT_QUERY)
                .prepare()
                .setQueryParameter(1, listingsStr, JDBCType.VARCHAR)
                .setQueryParameter(2, marketSourceName, JDBCType.VARCHAR)
                .setQueryParameter(3, listingsStr, JDBCType.VARCHAR);

        return this;
    }

    /**
     * Deletes the listings for a market.
     * The market source name must be set before calling delete. MissingFieldException will be thrown if it isn't.
     *
     * @return Count of affected records.
     * @throws Exception
     */
    @Override
    public ListingsDao<T> delete() throws Exception {
        if (null == marketSourceName) {
            throw MARKET_SOURCE_MISSING;
        }

        connection.newQuery(DELETE_QUERY)
                .prepare()
                .setQueryParameter(1, marketSourceName, JDBCType.VARCHAR);

        return this;
    }

}
