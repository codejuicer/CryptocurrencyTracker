package com.codejuicer;

import com.codejuicer.dao.AbstractDao;
import com.codejuicer.dao.CryptoRatesDao;
import com.codejuicer.models.gson.coinmarketcap.CoinMarketCapTickerArray;
import com.codejuicer.rest.coinmarketcap.CoinMarketCapEndpoint;
import com.codejuicer.rest.coinmarketcap.TickerParameters;
import com.codejuicer.util.ResourceManager;

public class RateWatcher {
    private static ResourceManager<String> stringResources = new ResourceManager<>();
    private static AbstractDao<CoinMarketCapTickerArray.Data> ratesDao;    
    
    private RateWatcher() throws Exception {
        stringResources.createStore("config");
        stringResources.getStore("config").addResource("DatabaseDriver", "com.mysql.cj.jdbc.Driver");
        stringResources.getStore("config").addResource("DatabaseConnection",
                String.format("jdbc:mysql://%s/cryptocurrency?user=%s&password=%s" +
                                "&useSSL=true&verifyServerCertificate=false" +
                                "&rewriteBatchedStatements=true&useServerPrepStmts=false",
                        System.getProperty("MYSQL_HOST"),
                        System.getProperty("MYSQL_USER"),
                        System.getProperty("MYSQL_PASS")));

        ratesDao = new CryptoRatesDao<>(CoinMarketCapTickerArray.Data.class)
                .withDriver(stringResources.getStore("config").getResource("DatabaseDriver"))
                .withConnection(stringResources.getStore("config").getResource("DatabaseConnection"));
        
    }
    
    private void run() throws Exception {
        CoinMarketCapEndpoint<CoinMarketCapTickerArray> endpoint = new CoinMarketCapEndpoint<>(CoinMarketCapTickerArray.class);
        CoinMarketCapTickerArray ticker = null;
        int recordStart = 1;
        
        TickerParameters params = new TickerParameters().setStart(recordStart);
        // Only really care about more records if a limit is set. Otherwise it assumes all records returned.
        // Basically pagination support.
        boolean hasMore = null != params.getLimit();

        // Initialize the DAO.
        ((CryptoRatesDao<CoinMarketCapTickerArray.Data>)ratesDao)
                .init()
                .withMarketSource("Coin Market Cap")
                .asBatch();

        System.out.println("Fetching rates...");
        // Needs to run at least once.
        do {
            ticker = (CoinMarketCapTickerArray) endpoint.init()
                    .withMethod(CoinMarketCapEndpoint.TICKER_METHOD)
                    .withResource(System.getProperty("ResourceId"))
                    .withParameters(new TickerParameters().setStart(recordStart))
                    .call();
            
            // Process any records found.
            if(null != ticker) {
                recordStart += (null == params.getLimit() ? 0 : params.getLimit());
                
                // Append each record to the batch.
                ticker.getData().forEach(rate -> {
                    try {
                        ((CryptoRatesDao<CoinMarketCapTickerArray.Data>)ratesDao)
                                .withRates(rate)
                                .addToBatch();                         
                    } catch (Exception putExc) {
                        System.err.println(putExc.getLocalizedMessage());
                    }
                });
                
                if(ticker.getData().size() < 2) {
                    hasMore = false;
                }
            } else
                hasMore = false;
        } while(hasMore);
        
        // Ready to insert. So do it.
        System.out.println("Inserting rates...");
        System.out.println("Inserted " + ratesDao.execute());
        System.out.println("Done.");
    }
    

    public static void main(String[] args) {
        try {
            RateWatcher rateWatcher = new RateWatcher();
            rateWatcher.run();
        } catch(Exception exc) {
            System.err.println(exc.getLocalizedMessage());
        }
        
    }
}
