package com.codejuicer.models.gson.solopool;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;
import java.util.List;
import java.util.Map;

@Generated("io.t28.json2java.core.JavaConverter")
@SuppressWarnings("all")
public class SolopoolStats {
    @SerializedName("blocksFound24")
    private final int blocksFound24;

    @SerializedName("charts")
    private final List<Charts> charts;

    @SerializedName("currentHashrate")
    private final int currentHashrate;

    @SerializedName("earnings")
    private final List<Earnings> earnings;

    @SerializedName("hashrate")
    private final int hashrate;

    @SerializedName("payments")
    private final List<Payments> payments;

    @SerializedName("paymentsTotal")
    private final int paymentsTotal;

    @SerializedName("reward24")
    private final long reward24;

    @SerializedName("rewards")
    private final List<Rewards> rewards;

    @SerializedName("stats")
    private final Stats stats;
    
    @SerializedName("workers")
    private Map<String, WorkerStats> workers;

    @SerializedName("workersOffline")
    private final int workersOffline;

    @SerializedName("workersOnline")
    private final int workersOnline;

    @SerializedName("workersTotal")
    private final int workersTotal;

    public SolopoolStats(int blocksFound24, List<Charts> charts, int currentHashrate,
                         List<Earnings> earnings, int hashrate, List<Payments> payments, int paymentsTotal,
                         long reward24, List<Rewards> rewards, Stats stats, Map<String, WorkerStats> workers, int workersOffline,
                         int workersOnline, int workersTotal) {
        this.blocksFound24 = blocksFound24;
        this.charts = charts;
        this.currentHashrate = currentHashrate;
        this.earnings = earnings;
        this.hashrate = hashrate;
        this.payments = payments;
        this.paymentsTotal = paymentsTotal;
        this.reward24 = reward24;
        this.rewards = rewards;
        this.stats = stats;
        this.workers = workers;
        this.workersOffline = workersOffline;
        this.workersOnline = workersOnline;
        this.workersTotal = workersTotal;
    }

    public int getBlocksFound24() {
        return blocksFound24;
    }

    public List<Charts> getCharts() {
        return charts;
    }

    public int getCurrentHashrate() {
        return currentHashrate;
    }

    public List<Earnings> getEarnings() {
        return earnings;
    }

    public int getHashrate() {
        return hashrate;
    }

    public List<Payments> getPayments() {
        return payments;
    }

    public int getPaymentsTotal() {
        return paymentsTotal;
    }

    public long getReward24() {
        return reward24;
    }

    public List<Rewards> getRewards() {
        return rewards;
    }

    public Stats getStats() {
        return stats;
    }
    
    public Map<String, WorkerStats> getWorkers() { return workers; }

    public int getWorkersOffline() {
        return workersOffline;
    }

    public int getWorkersOnline() {
        return workersOnline;
    }

    public int getWorkersTotal() {
        return workersTotal;
    }

    public static class Charts {
        @SerializedName("x")
        private final int x;

        @SerializedName("y")
        private final int y;

        public Charts(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }
    }

    public static class Earnings {
        @SerializedName("period")
        private final int period;

        @SerializedName("amount")
        private final long amount;

        @SerializedName("blocks")
        private final int blocks;

        public Earnings(int period, long amount, int blocks) {
            this.period = period;
            this.amount = amount;
            this.blocks = blocks;
        }

        public int getPeriod() {
            return period;
        }

        public long getAmount() {
            return amount;
        }

        public int getBlocks() {
            return blocks;
        }
    }

    public static class Payments {
        @SerializedName("amount")
        private final long amount;

        @SerializedName("timestamp")
        private final int timestamp;

        @SerializedName("tx")
        private final String tx;

        public Payments(long amount, int timestamp, String tx) {
            this.amount = amount;
            this.timestamp = timestamp;
            this.tx = tx;
        }

        public long getAmount() {
            return amount;
        }

        public int getTimestamp() {
            return timestamp;
        }

        public String getTx() {
            return tx;
        }
    }

    public static class Rewards {
        @SerializedName("height")
        private final int height;

        @SerializedName("timestamp")
        private final int timestamp;

        @SerializedName("amount")
        private final long amount;

        @SerializedName("matured")
        private final boolean matured;

        @SerializedName("orphan")
        private final boolean orphan;

        public Rewards(int height, int timestamp, long amount, boolean matured, boolean orphan) {
            this.height = height;
            this.timestamp = timestamp;
            this.amount = amount;
            this.matured = matured;
            this.orphan = orphan;
        }

        public int getHeight() {
            return height;
        }

        public int getTimestamp() {
            return timestamp;
        }

        public long getAmount() {
            return amount;
        }

        public boolean isMatured() {
            return matured;
        }

        public boolean isOrphan() {
            return orphan;
        }
    }

    public static class Stats {
        @SerializedName("balance")
        private final long balance;

        @SerializedName("blocksFound")
        private final int blocksFound;

        @SerializedName("immature")
        private final long immature;

        @SerializedName("lastShare")
        private final int lastShare;

        @SerializedName("paid")
        private final long paid;

        @SerializedName("pending")
        private final int pending;

        public Stats(long balance, int blocksFound, long immature, int lastShare, long paid,
                     int pending) {
            this.balance = balance;
            this.blocksFound = blocksFound;
            this.immature = immature;
            this.lastShare = lastShare;
            this.paid = paid;
            this.pending = pending;
        }

        public long getBalance() {
            return balance;
        }

        public int getBlocksFound() {
            return blocksFound;
        }

        public long getImmature() {
            return immature;
        }

        public int getLastShare() {
            return lastShare;
        }

        public long getPaid() {
            return paid;
        }

        public int getPending() {
            return pending;
        }
    }
    
    public static class WorkerStats {
        @SerializedName("lastBeat")
        private final long lastBeat;
        
        @SerializedName("hr")
        private final long hr;
        
        @SerializedName("offline")
        private final boolean offline;
        
        @SerializedName("hr2")
        private final long hr2;

        public WorkerStats(long lastBeat, long hr, boolean offline, long hr2) {
            this.lastBeat = lastBeat;
            this.hr = hr;
            this.offline = offline;
            this.hr2 = hr2;
        }

        public long getLastBeat() {
            return lastBeat;
        }

        public long getHr() {
            return hr;
        }

        public boolean isOffline() {
            return offline;
        }

        public long getHr2() {
            return hr2;
        }
    }
}
