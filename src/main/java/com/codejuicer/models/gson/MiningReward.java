package com.codejuicer.models.gson;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

/**
 * GSON representaiton of the mining reward table JSON object.
 */
@Generated("io.t28.json2java.core.JavaConverter")
@SuppressWarnings("all")
public class MiningReward {
    @SerializedName("rewardSource")
    private final String rewardSource;

    @SerializedName("destinationAccount")
    private final String destinationAccount;

    @SerializedName("amount")
    private final double amount;

    @SerializedName("transaction")
    private final String transaction;

    @SerializedName("timestamp")
    private final long timestamp;

    public MiningReward(String rewardSource, String destinationAccount, double amount,
                        String transaction, long timestamp) {
        this.rewardSource = rewardSource;
        this.destinationAccount = destinationAccount;
        this.amount = amount;
        this.transaction = transaction;
        this.timestamp = timestamp;
    }

    public String getRewardSource() {
        return rewardSource;
    }

    public String getDestinationAccount() {
        return destinationAccount;
    }

    public double getAmount() {
        return amount;
    }

    public String getTransaction() {
        return transaction;
    }

    public long getTimestamp() {
        return timestamp;
    }
}
