package com.codejuicer.models.gson.coinmarketcap;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class CoinMarketCapListingDatum {

    @SerializedName("id")
    private Long mId;
    @SerializedName("name")
    private String mName;
    @SerializedName("symbol")
    private String mSymbol;
    @SerializedName("website_slug")
    private String mWebsiteSlug;

    public Long getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public String getSymbol() {
        return mSymbol;
    }

    public String getWebsiteSlug() {
        return mWebsiteSlug;
    }

    public static class Builder {

        private Long mId;
        private String mName;
        private String mSymbol;
        private String mWebsiteSlug;

        public CoinMarketCapListingDatum.Builder withId(Long id) {
            mId = id;
            return this;
        }

        public CoinMarketCapListingDatum.Builder withName(String name) {
            mName = name;
            return this;
        }

        public CoinMarketCapListingDatum.Builder withSymbol(String symbol) {
            mSymbol = symbol;
            return this;
        }

        public CoinMarketCapListingDatum.Builder withWebsiteSlug(String websiteSlug) {
            mWebsiteSlug = websiteSlug;
            return this;
        }

        public CoinMarketCapListingDatum build() {
            CoinMarketCapListingDatum listingDatum = new CoinMarketCapListingDatum();
            listingDatum.mId = mId;
            listingDatum.mName = mName;
            listingDatum.mSymbol = mSymbol;
            listingDatum.mWebsiteSlug = mWebsiteSlug;
            return listingDatum;
        }

    }

}
