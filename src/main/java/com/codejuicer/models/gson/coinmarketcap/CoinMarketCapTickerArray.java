package com.codejuicer.models.gson.coinmarketcap;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;
import java.util.List;
import java.util.Map;

@Generated("io.t28.json2java.core.JavaConverter")
@SuppressWarnings("all")
public class CoinMarketCapTickerArray {
    @SerializedName("data")
    private final List<Data> data;

    @SerializedName("metadata")
    private final Metadata metadata;

    public CoinMarketCapTickerArray(List<Data> data, Metadata metadata) {
        this.data = data;
        this.metadata = metadata;
    }

    public List<Data> getData() {
        return data;
    }
    

    public Metadata getMetadata() {
        return metadata;
    }

    public static class Data {
        @SerializedName("id")
        private final int id;

        @SerializedName("name")
        private final String name;

        @SerializedName("symbol")
        private final String symbol;

        @SerializedName("website_slug")
        private final String websiteSlug;

        @SerializedName("rank")
        private final int rank;

        @SerializedName("circulating_supply")
        private final double circulatingSupply;

        @SerializedName("total_supply")
        private final double totalSupply;

        @SerializedName("max_supply")
        private final double maxSupply;

        @SerializedName("quotes")
        private final Map<String, PricingInfo> quotes;

        @SerializedName("last_updated")
        private final int lastUpdated;

        public Data(int id, String name, String symbol, String websiteSlug, int rank,
                    double circulatingSupply, double totalSupply, double maxSupply, Map<String, PricingInfo> quotes,
                    int lastUpdated) {
            this.id = id;
            this.name = name;
            this.symbol = symbol;
            this.websiteSlug = websiteSlug;
            this.rank = rank;
            this.circulatingSupply = circulatingSupply;
            this.totalSupply = totalSupply;
            this.maxSupply = maxSupply;
            this.quotes = quotes;
            this.lastUpdated = lastUpdated;
        }

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getSymbol() {
            return symbol;
        }

        public String getWebsiteSlug() {
            return websiteSlug;
        }

        public int getRank() {
            return rank;
        }

        public double getCirculatingSupply() {
            return circulatingSupply;
        }

        public double getTotalSupply() {
            return totalSupply;
        }

        public double getMaxSupply() {
            return maxSupply;
        }

        public Map<String, PricingInfo> getQuotes() {
            return quotes;
        }

        public int getLastUpdated() {
            return lastUpdated;
        }
    }

    public static class PricingInfo {
        @SerializedName("price")
        private final double price;

        @SerializedName("volume_24h")
        private final double volume24h;

        @SerializedName("market_cap")
        private final double marketCap;

        @SerializedName("percent_change_1h")
        private final double percentChange1h;

        @SerializedName("percent_change_24h")
        private final double percentChange24h;

        @SerializedName("percent_change_7d")
        private final double percentChange7d;

        public PricingInfo(double price, double volume24h, double marketCap, double percentChange1h,
                           double percentChange24h, double percentChange7d) {
            this.price = price;
            this.volume24h = volume24h;
            this.marketCap = marketCap;
            this.percentChange1h = percentChange1h;
            this.percentChange24h = percentChange24h;
            this.percentChange7d = percentChange7d;
        }

        public double getPrice() {
            return price;
        }

        public double getVolume24h() {
            return volume24h;
        }

        public double getMarketCap() {
            return marketCap;
        }

        public double getPercentChange1h() {
            return percentChange1h;
        }

        public double getPercentChange24h() {
            return percentChange24h;
        }

        public double getPercentChange7d() {
            return percentChange7d;
        }
    }    

    public static class Metadata {
        @SerializedName("timestamp")
        private final int timestamp;

        @SerializedName("num_cryptocurrencies")
        private final int numCryptocurrencies;

        @SerializedName("error")
        private final Object error;

        public Metadata(int timestamp, int numCryptocurrencies, Object error) {
            this.timestamp = timestamp;
            this.numCryptocurrencies = numCryptocurrencies;
            this.error = error;
        }

        public int getTimestamp() {
            return timestamp;
        }

        public int getNumCryptocurrencies() {
            return numCryptocurrencies;
        }

        public Object getError() {
            return error;
        }
    }
}
