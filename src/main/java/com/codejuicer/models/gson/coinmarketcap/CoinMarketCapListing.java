package com.codejuicer.models.gson.coinmarketcap;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class CoinMarketCapListing {
    public static int LISTING_COLUMN = 1;

    @SerializedName("data")
    private List<CoinMarketCapListingDatum> mData;
    @SerializedName("metadata")
    private Metadata mMetadata;
    
    public static final String fields = "data,id,name,symbol,webside_slug,metadata,timestamp,num_cryptocurrencies,error";

    public List<CoinMarketCapListingDatum> getData() {
        return mData;
    }

    public Metadata getMetadata() {
        return mMetadata;
    }

    public static class Builder {

        private List<CoinMarketCapListingDatum> mData;
        private Metadata mMetadata;

        public CoinMarketCapListing.Builder withData(List<CoinMarketCapListingDatum> data) {
            mData = data;
            return this;
        }

        public CoinMarketCapListing.Builder withMetadata(Metadata metadata) {
            mMetadata = metadata;
            return this;
        }

        public CoinMarketCapListing build() {
            CoinMarketCapListing coinMarketCapListing = new CoinMarketCapListing();
            coinMarketCapListing.mData = mData;
            coinMarketCapListing.mMetadata = mMetadata;
            return coinMarketCapListing;
        }

    }

}
