package com.codejuicer.models.gson.coinmarketcap;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Metadata {

    @SerializedName("error")
    private Object mError;
    @SerializedName("num_cryptocurrencies")
    private Long mNumCryptocurrencies;
    @SerializedName("timestamp")
    private Long mTimestamp;

    public Object getError() {
        return mError;
    }

    public Long getNumCryptocurrencies() {
        return mNumCryptocurrencies;
    }

    public Long getTimestamp() {
        return mTimestamp;
    }

    public static class Builder {

        private Object mError;
        private Long mNumCryptocurrencies;
        private Long mTimestamp;

        public Metadata.Builder withError(Object error) {
            mError = error;
            return this;
        }

        public Metadata.Builder withNumCryptocurrencies(Long numCryptocurrencies) {
            mNumCryptocurrencies = numCryptocurrencies;
            return this;
        }

        public Metadata.Builder withTimestamp(Long timestamp) {
            mTimestamp = timestamp;
            return this;
        }

        public Metadata build() {
            Metadata metadata = new Metadata();
            metadata.mError = mError;
            metadata.mNumCryptocurrencies = mNumCryptocurrencies;
            metadata.mTimestamp = mTimestamp;
            return metadata;
        }

    }

}
