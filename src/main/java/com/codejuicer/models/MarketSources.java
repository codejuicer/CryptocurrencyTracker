package com.codejuicer.models;

import com.google.gson.annotations.SerializedName;

public class MarketSources {
    @SerializedName("market_source_id")
    private final int marketSourceId;
    
    @SerializedName("market_source_name")
    private final String marketSourceName;
    
    @SerializedName("api_url")
    private final String apiUrl;
    
    public MarketSources(int marketSourceId, String marketSourceName, String apiUrl) {
        this.marketSourceId = marketSourceId;
        this.marketSourceName = marketSourceName;
        this.apiUrl = apiUrl;
    }

    public int getMarketSourceId() {
        return marketSourceId;
    }

    public String getMarketSourceName() {
        return marketSourceName;
    }

    public String getApiUrl() {
        return apiUrl;
    }
}
